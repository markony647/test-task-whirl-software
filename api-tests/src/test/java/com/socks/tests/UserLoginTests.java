package com.socks.tests;

import com.socks.api.EndPoints;
import com.socks.api.models.User;
import com.socks.api.services.UserApiService;
import com.socks.api.test_data_providers.TestData;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.not;

public class UserLoginTests {

   UserApiService userApiService = new UserApiService();

   @BeforeAll
   public static void setUp() {
      RestAssured.baseURI = "http://" + EndPoints.base;
   }

   @Test
   public void testCanLoginWithValidCredentials() {
      User user = TestData.getUserProvider().getExistingUser();
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(200)
          .header("Content-Length", is(not(String.valueOf(0))));
   }

   @Test
   public void testCanLoginJustRegisteredUser() {
      User user = TestData.getUserProvider().getUserWithAllValidFields();
      userApiService.registerUser(user);
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(200)
          .header("Content-Length", is(not(String.valueOf(0))));
   }

   @Test
   public void testCanNotLoginWithoutPassword() {
      User user = TestData.getUserProvider()
          .getExistingUser()
          .setPassword("");
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(401)
          .statusLine("HTTP/1.1 401 Unauthorized");
   }

   @Test
   public void testCanNotLoginWithoutUsername() {
      User user = TestData.getUserProvider()
          .getExistingUser()
          .setUsername("");
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(401)
          .statusLine("HTTP/1.1 401 Unauthorized");
   }

   @Test
   public void testCanNotLoginWithInvalidPassword() {
      User user = TestData.getUserProvider()
          .getExistingUser()
          .setPassword("invalid");
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(401)
          .statusLine("HTTP/1.1 401 Unauthorized");
   }

   @Test
   public void testCanNotLoginWithInvalidUsername() {
      User user = TestData.getUserProvider()
          .getExistingUser()
          .setUsername("invalid");
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(401)
          .statusLine("HTTP/1.1 401 Unauthorized");
   }

   @Test
   public void testCanNotLoginWithoutCredentials() {
      User user = TestData.getUserProvider()
          .getExistingUser()
          .setUsername("")
          .setPassword("");
      ValidatableResponse response = userApiService.login(user);

      response.assertThat()
          .statusCode(401)
          .statusLine("HTTP/1.1 401 Unauthorized");
   }
}
