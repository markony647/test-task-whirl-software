package com.socks.tests;

import com.socks.api.EndPoints;
import com.socks.api.models.Product;
import com.socks.api.services.ProductApiService;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CartAddingTests {

   ProductApiService productApiService = new ProductApiService();

   @BeforeAll
   public static void setUp() {
      RestAssured.baseURI = "http://" + EndPoints.base;
   }


   @Test
   public void testGuestUserCanAddProductToCart() {
      List<Product> allProducts = productApiService.getAllProducts();
      Product product = allProducts.get(0);
      ValidatableResponse response = productApiService.addProductToCart(product);
      response.assertThat()
          .statusCode(201)
          .statusLine("HTTP/1.1 201 Created");
   }
}
