package com.socks.tests;

import com.socks.api.EndPoints;
import com.socks.api.models.User;
import com.socks.api.services.UserApiService;
import com.socks.api.test_data_providers.TestData;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.isEmptyString;

public class UserRegistrationTests {

   UserApiService userApiService = new UserApiService();

   @BeforeAll
   public static void setUp() {
      RestAssured.baseURI = "http://" + EndPoints.base;
   }

   @Test
   public void testCanRegisterUserWithValidCredentials() {
      User user = TestData.getUserProvider().getUserWithAllValidFields();
      ValidatableResponse response = userApiService.registerUser(user);

      response.assertThat()
          .statusCode(200)
          .body("id", not(isEmptyString()));
   }

   @Test
   public void testCanRegisterUserWithOnlyRequiredValidFields() {
      User user = TestData.getUserProvider().getUserWithOnlyRequiredValidFields();
      ValidatableResponse response = userApiService.registerUser(user);
      response.assertThat()
          .statusCode(200)
          .body("id", not(isEmptyString()));
   }

   @Test
   public void testCanNotRegisterWithoutUsername() {
      User user = TestData.getUserProvider()
          .getUserWithOnlyRequiredValidFields()
          .setUsername(null);

      ValidatableResponse response = userApiService.registerUser(user);
      response.assertThat().statusCode(500)
          .statusLine("HTTP/1.1 500 Internal Server Error")
          .header("Content-Length", is(String.valueOf(0)));
   }

   @Test
   public void testCanNotRegisterWithoutEmail() {
      User user = TestData.getUserProvider()
          .getUserWithOnlyRequiredValidFields()
          .setEmail(null);

      ValidatableResponse response = userApiService.registerUser(user);
      response.assertThat().statusCode(500)
          .statusLine("HTTP/1.1 500 Internal Server Error")
          .header("Content-Length", is(String.valueOf(0)));
   }

   @Test
   public void testCanNotRegisterWithoutPassword() {
      User user = TestData.getUserProvider()
          .getUserWithOnlyRequiredValidFields()
          .setPassword(null);

      ValidatableResponse response = userApiService.registerUser(user);
      response.assertThat().statusCode(500)
          .statusLine("HTTP/1.1 500 Internal Server Error")
          .header("Content-Length", is(String.valueOf(0)));
   }
}
