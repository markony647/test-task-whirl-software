package com.socks.api.services;

import com.socks.api.EndPoints;
import com.socks.api.models.User;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserApiService {

   @Step
   public RequestSpecification setUp() {
      return RestAssured.given().contentType(ContentType.JSON)
          .filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
   }

   @Step
   public ValidatableResponse registerUser(User user) {
      log.info("register user {}", user);
      return setUp()
          .body(user)
          .when()
          .post(EndPoints.registerUser)
          .then();
   }

   @Step
   public ValidatableResponse login(User user) {
      String username = user.getUsername();
      String password = user.getPassword();
      log.info("logging with credentials: {} / {}", username, password);
      return setUp()
            .auth().preemptive()
            .basic(username, password)
            .when()
            .get(EndPoints.login)
            .then();
   }
}
