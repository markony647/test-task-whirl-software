package com.socks.api.services;

import com.socks.api.EndPoints;
import com.socks.api.models.Product;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ProductApiService {

   @Step
   public RequestSpecification setUp() {
      return RestAssured.given().contentType(ContentType.JSON)
          .filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
   }

   @Step
   public List<Product> getAllProducts() {
      return setUp()
            .when()
            .get(EndPoints.catalogue)
            .jsonPath()
            .getList("", Product.class);
   }

   @Step
   public ValidatableResponse addProductToCart(Product product) {
      String id = product.getId();
      Map<String, String> payload = new HashMap<>();
      log.info("adding product with id: {} to cart", id);
      payload.put("id", id);
      return setUp()
          .body(payload)
          .post(EndPoints.addToCart)
          .then();
   }
}
