package com.socks.api.models;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class User {
   private String firstName;
   private String lastName;
   private String password;
   private String email;
   private String username;

   @Override
   public String toString() {
      return "User{" +
          "firstName='" + firstName + '\'' +
          ", lastName='" + lastName + '\'' +
          ", password='" + password + '\'' +
          ", email='" + email + '\'' +
          ", username='" + username + '\'' +
          '}';
   }
}
