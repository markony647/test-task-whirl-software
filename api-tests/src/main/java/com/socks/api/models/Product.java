package com.socks.api.models;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Product {
   private String id;
   private String name;
   private String description;
   private List<String> imageUrl;
   private double price;
   private int count;
   private List<String> tag;
}
