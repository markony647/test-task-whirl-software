package com.socks.api.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.socks.api.models.User;
import com.socks.api.utils.StringUtil;

import java.io.File;
import java.io.IOException;

public class YamlHelper {
   private ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
   private String basePath = "../resources/";
   private String yamlFileName = "user_all_fields_valid.yaml";

   public User buildUser() {
      try {
         User user = mapper.readValue(new File(basePath + yamlFileName), User.class);
         return user.setUsername(StringUtil.generateRandomName());
      } catch (IOException e) {
         throw new RuntimeException("Can not create user");
      }
   }
}
