package com.socks.api.test_data_providers;

import com.socks.api.helpers.YamlHelper;
import com.socks.api.models.User;

public class UserProvider {

   YamlHelper yamlHelper = new YamlHelper();

   public User getUserWithAllValidFields() {
      return yamlHelper.buildUser();
   }

   public User getUserWithOnlyRequiredValidFields() {
      return yamlHelper.buildUser()
          .setFirstName(null)
          .setLastName(null);
   }

   public User getExistingUser() {
      return yamlHelper.buildUser()
          .setUsername("user")
          .setPassword("password");
   }
}
