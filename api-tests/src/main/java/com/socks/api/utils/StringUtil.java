package com.socks.api.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class StringUtil {

   public static String generateRandomName() {
      int numOfCharsInName = 6;
      return RandomStringUtils.randomAlphanumeric(numOfCharsInName);
   }
}
