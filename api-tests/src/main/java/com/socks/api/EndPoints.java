package com.socks.api;

public final class EndPoints {
   public static final String base = "127.0.0.1";
   public static final String registerUser = "/register";
   public static final String login = "/login";
   public static final String catalogue = "/catalogue";
   public static final String addToCart = "/cart";
}
