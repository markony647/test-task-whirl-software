#Sample Testing Project

This is an educational Java based project for web application testing.
It is not covering all features, just several tests for some basic app features.
 
Software under test (SUT) is https://github.com/microservices-demo/microservices-demo

##SUT Deployment
Follow the instructions to deploy a project https://microservices-demo.github.io/deployment/docker-compose.html


In current project I suppose that SUT is deployed locally.

##Structure
The project contains 2 modules: for API and UI tests.

###To run
```
gradle api-tests:test
```

Will run API tests (NOTE: some tests are failing due to the bugs in SUT)
#####Note: To run from IDE (ex. Intellij), you should turn on Annotation processing to make it work with
Lombok https://stackoverflow.com/questions/24006937/lombok-annotations-do-not-compile-under-intellij-idea

```
gradle ui-tests:test
```
Will run UI tests using Chrome browser.

###Reports
You can generate a report after the test execution.
To do this, just perform
```
gradle ui-tests:clean
gradle ui-tests:test
gradle ui-tests:allureReport
```


