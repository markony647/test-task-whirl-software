package com.socks.ui.tests;

import com.socks.api.models.User;
import com.socks.api.test_data_providers.TestData;
import com.socks.ui.kibana.KibanaTestListener;
import com.socks.ui.pages.MainPage;
import com.socks.ui.pages.modals.LoginModal;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;

@Listeners(KibanaTestListener.class)
public class UserLoginTests extends BaseTest {

   @Test
   public void testCanLoginWithValidCredentials() {
      User user = TestData.getUserProvider().getExistingUser();

      MainPage.visit()
          .openLoginForm();

      at(LoginModal.class)
          .submitFormFor(user);

      at(MainPage.class).loggedUserLabel.shouldHave(exactText("Logged in as User Name"));
   }

   @Test
   public void testCanNotLoginWithInvalidPassword() {
      User user = TestData.getUserProvider().getExistingUser();
      user.setPassword("invalid");

      MainPage.visit()
          .openLoginForm();

      at(LoginModal.class)
          .submitFormFor(user);

      at(LoginModal.class).invalidLoginAlert.shouldHave(exactText("Invalid login credentials."));
   }
}
