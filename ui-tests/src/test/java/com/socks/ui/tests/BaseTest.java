package com.socks.ui.tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Step;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;


public class BaseTest {

   @BeforeSuite
   public static void setUp() {
      Configuration.browser = "chrome";
      Configuration.browserSize = "1920x1080";
      Configuration.baseUrl = "http://127.0.0.1";
   }

   @BeforeMethod
   public void clearBrowser() {
      WebDriverRunner.clearBrowserCache();
   }


   @Step
   protected <T> T at(Class<T> tClass) {
      try {
         return tClass.newInstance();
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
   }
}
