package com.socks.ui.tests;

import com.socks.ui.kibana.KibanaTestListener;
import com.socks.ui.pages.MainPage;
import com.socks.ui.pages.ProductsPage;
import com.socks.ui.pages.ShoppingCartPage;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.value;

@Listeners(KibanaTestListener.class)
public class CartTests extends BaseTest {

   @Test
   public void testUserCanAddProductToCart() {
      MainPage.visit()
          .openCatalogue();

      at(ProductsPage.class)
          .addNthProductToCart(1)
          .numInCartCounter.shouldHave(exactText("1 item(s) in cart"));

      at(ProductsPage.class)
          .openCart();

      at(ShoppingCartPage.class)
          .itemsToBuy.shouldHave(size(1));
      at(ShoppingCartPage.class)
          .quantity.first().shouldHave(value(String.valueOf(1)));
   }
}
