package com.socks.ui.tests;

import com.socks.api.models.User;
import com.socks.api.test_data_providers.TestData;
import com.socks.ui.kibana.KibanaTestListener;
import com.socks.ui.pages.MainPage;
import com.socks.ui.pages.modals.RegistrationModal;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.exactText;

@Listeners(KibanaTestListener.class)
public class RegistrationTests extends BaseTest {

   @Test
   public void testCanRegisterNewUser() {
      User user = TestData.getUserProvider().getUserWithAllValidFields();
      String loginExpectedText = String.format("Logged in as %s %s", user.getFirstName(), user.getLastName());
      MainPage.visit()
          .openRegistrationModal();

      at(RegistrationModal.class)
          .submitFromFor(user);

      at(MainPage.class).loggedUserLabel.shouldHave(exactText(loginExpectedText));
   }
}
