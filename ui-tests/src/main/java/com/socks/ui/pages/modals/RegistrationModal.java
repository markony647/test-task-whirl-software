package com.socks.ui.pages.modals;

import com.codeborne.selenide.SelenideElement;
import com.socks.api.models.User;
import lombok.extern.slf4j.Slf4j;

import static com.codeborne.selenide.Selenide.$;

@Slf4j
public class RegistrationModal {

   private SelenideElement userNameField =  $("#register-username-modal");

   public void submitFromFor(User user) {
      log.info("register user {}", user);
      userNameField.click();
      userNameField.setValue(user.getUsername());
      $("#register-first-modal").setValue(user.getFirstName());
      $("#register-last-modal").setValue(user.getLastName());
      $("#register-email-modal").setValue(user.getEmail());
      $("#register-password-modal").setValue(user.getPassword());
      $("#register-modal .modal-body button").click();
   }
}
