package com.socks.ui.pages;

import com.codeborne.selenide.SelenideElement;
import com.socks.ui.pages.modals.LoginModal;
import com.socks.ui.pages.modals.RegistrationModal;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPage {

   public SelenideElement loggedUserLabel = $("#howdy > a");

   public static MainPage visit() {
      return open("/", MainPage.class);
   }

   public RegistrationModal openRegistrationModal() {
      $("#register > a").click();
      return new RegistrationModal();
   }

   public LoginModal openLoginForm() {
      $("#login").click();
      return new LoginModal();
   }

   public void openCatalogue() {
      $(".dropdown.yamm-fw").click();
   }
}
