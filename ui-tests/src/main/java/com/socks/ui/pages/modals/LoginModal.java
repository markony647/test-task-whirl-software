package com.socks.ui.pages.modals;

import com.codeborne.selenide.SelenideElement;
import com.socks.api.models.User;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {

   public SelenideElement invalidLoginAlert = $(".alert-danger");
   private SelenideElement userNameField = $("#username-modal");

   public void submitFormFor(User user) {
      userNameField.click();
      userNameField.setValue(user.getUsername());
      $("#password-modal").setValue(user.getPassword());
      $("#login-modal .btn").click();
   }
}
