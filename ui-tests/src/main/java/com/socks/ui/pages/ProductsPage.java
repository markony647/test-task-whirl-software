package com.socks.ui.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductsPage {

   public ElementsCollection products = $$(".product");
   public SelenideElement numInCartCounter = $("#numItemsInCart");

   public ProductsPage addNthProductToCart(int index) {
      index -= 1;
      products.get(index).find(".btn-primary").click();
      return this;
   }

   public void openCart() {
      numInCartCounter.click();
   }
}
