package com.socks.ui.pages;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.$$;

public class ShoppingCartPage {

   public ElementsCollection itemsToBuy = $$("tr.item");
   public ElementsCollection quantity = $$("[type=number]");
}
