package com.socks.ui.kibana;

public enum Status {
    PASSED,
    FAILED,
    SKIPPED
}
