package com.socks.ui.kibana;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResultSender {

    private static final ObjectMapper OM = new ObjectMapper();
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String ELASTICSEARCH_URL = "http://46.101.200.250:9200/app/suite";

    public static void send(TestResult testResult) {
        try {
            Unirest.post(ELASTICSEARCH_URL)
                    .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                    .body(OM.writeValueAsString(testResult)).asJson();
            log.info("Result send to Kibana");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
