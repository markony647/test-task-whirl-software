package com.socks.ui.kibana;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.Instant;
import java.time.LocalDateTime;

public class KibanaTestListener implements ITestListener {

    private TestResult testResult;

    @Override
    public void onTestStart(ITestResult result) {
        testResult = new TestResult();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        sendStatus(result, Status.PASSED);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        sendStatus(result, Status.FAILED);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        sendStatus(result, Status.SKIPPED);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        throw new NotImplementedException();
    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }

    public void sendStatus(ITestResult iTestResult, Status status) {
        testResult.setStatus(status);
        testResult.setTestName(iTestResult.getMethod().getMethodName());
        testResult.setTestClass(iTestResult.getTestClass().getName());
        testResult.setExecutionTime(Instant.now().toEpochMilli());
        ResultSender.send(testResult);
    }
}
