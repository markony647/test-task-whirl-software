package com.socks.ui.kibana;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Setter;

@Setter
public class TestResult {

    @JsonProperty
    private String testName;
    @JsonProperty
    private String testClass;
    @JsonProperty
    private Status status;
    @JsonProperty
    private long executionTime;
}
